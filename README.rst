SnoParser + Redis
=================

Parses the Server Notices from ErgoIRCd.

This branch contains Redis functionality:
Performs and caches WHOIS lookups for connecting IP addresses. Counts nicknames and IP addresses.


Assumptions
-----------

- ``force-nick-equals-account=True``
- The Bot needs to be oper and have all the snomasks.

Current Coverage
----------------

1. CONNECT
2. XLINE
3. NICK
4. KILL
5. ACCOUNT (reg only)

Configurations
--------------

1. Formatting for 'AutoVhost' post Registration (TODO: Disable)
2. Configure the '#channel' to send the SNO lines (disabled by default) (TODO: Add Exceptions)
3. Use ZWSP to toggle nickhighlights when in channel.
